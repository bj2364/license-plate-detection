# Vehicle License Plate and Color Recognition using Computer Vision

Project as part of Dr. John Wright's ELEN 4810 - Digital Signal Processing course at Columbia University. Note that all the code was written in MATLAB 2016b.

A technical write up is included in the repository.

Description:

To view the complete project
1) Run "main.m"
2) Select an image in one of the folders that pop up
3) The command window will display the color of the car and the skew of the license plate. A window will pop up to show the isolated plate

To see step by step how "main.m" works,
1) Run "main_with_steps.m"
2) Select an image in one of the folders that pop up
3) Press the "enter" key to view the next step in the algorithm
4) After the last figure is displayed, the command window will display the color of hte car and the skew of the license plate

To see the emblem detection,
1) Go to "Emblem Detection" folder
2) Run "Emblem_Detection.m"
3) Select an image in one of the folders that pop up
4) An image of the logo will appear in a figure. The command window will display the manufacturer of the car

-Note: emblem detection only works for image 20


